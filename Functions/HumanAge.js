import React, { useState } from "react";

function HumanAge({age}) {
    
    return (
        <div>
            <h3>{age} human years</h3>
        </div>
    )
}

export default HumanAge