import React from "react";

function Tigra({name, addFamily}) {
    return (
        <div>
            <button onClick={addFamily}>+</button>
            {name}
        </div>
    )
}

export default  Tigra