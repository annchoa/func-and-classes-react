import React, {useState} from "react";
import Tigra from "./Tigra";
import Punsete from "./Punsete";
import HumanAge from "./HumanAge";

function FunctionApp() {
    const [newName, setName] = useState("Tigra")
    const cats = ["Seymour", "Bibi", "Sagan" , "Punsete", "Blanquito", "La gata de la vecina", "Nero"]
    const [age, setAge] = useState ()

   

    return (
        <div>
            <Tigra name={newName} addFamily={() => setName(newName + " " + cats[Math.floor(Math.random()*7)])}/>
            <Punsete pepe={setAge}/>
            <HumanAge age={age}/>
        </div>
    )
}

export default  FunctionApp