import React from "react";
import Pepita from "./Pepita";
import Manola from "./Manola";

class ClassesApp extends React.Component {
    constructor() {
        super()
        this.state = {
            name: "Pepita",
            gatos: 0
        }
    }

    setName(namePepita) {
        this.setState({name:namePepita})
    }

    render(){
    return(
    <div>
        <Pepita name={this.state.name} changeName={(namePepita) => this.setName(namePepita)}/>
        <Manola gatos={this.state.gatos} changeGatos={() => this.setState({gatos: this.state.gatos +1})}/>
    </div>
    )
    }
}

export default ClassesApp