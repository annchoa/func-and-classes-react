import React from "react";
import ReactDOM from "react-dom";
import ClassesApp from "./Classes/ClassesApp";
import FunctionApp from "./Functions/FunctionApp";


let mountNode = document.getElementById("app");
ReactDOM.render(
    <div>
        <ClassesApp/>
        <h1>Functions from here</h1>
        <FunctionApp/>
    </div>
    , mountNode)